import React, { Component } from 'react';
import './css/Video.css';
import background from '../img/Black.mp4';

export class Video extends Component {
    render() {
        return (
            <video autoPlay muted loop id="myVideo">
                <source src={background} type="video/mp4" />
            </video>
        );
    }
}

export default Video;
