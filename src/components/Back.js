import React, { Component } from 'react';
import './css/Back.css';
import backArrow from '../img/back.png';

export class Back extends Component {
    render() {
        return (
            <div className="back-arrow" onClick={() => this.props.delete()}>
                <img src={backArrow} alt="back" />
            </div>
        );
    }
}

export default Back;
