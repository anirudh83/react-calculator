import React, { Component } from 'react';
import './App.css';
import Button from './components/Button.js';
import Input from './components/Input.js';
import ClearButton from './components/ClearButton.js';
import Video from './components/Video.js';
import Back from './components/Back.js';
import * as math from 'mathjs';

export class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            input: ''
        };
    }

    addToInput = (val) => {
        if (this.state.input === 0) {
            this.setState({
                input: val[val.length - 1]
            });
        } else if (this.state.input === Infinity) {
            this.setState({
                input: val
            });
        } else if (
            this.state.input === '' ||
            this.state.input !== 'INVALID INPUT'
        ) {
            this.setState({ input: this.state.input + val });
        } else {
            this.setState({
                input: val
            });
        }
    };

    addZeoToInput = (val) => {
        if (this.state.input === Infinity) {
            this.setState({
                input: val
            });
        } else if (
            this.state.input !== '' &&
            this.state.input !== 'INVALID INPUT' &&
            this.state.input !== 0
        ) {
            this.setState({ input: this.state.input + val });
        }
    };

    addDecimal = (val) => {
        if (this.state.input === Infinity) {
            this.setState({
                input: val
            });
        } else {
            let lengthExp = this.state.input.toString().split(/[-,+,*,/]/)
                .length;
            let lastinput = this.state.input.toString().split(/[-,+,*,/]/)[
                lengthExp - 1
            ];

            if (lastinput.indexOf('.') === -1) {
                if (
                    this.state.input === '' ||
                    this.state.input !== 'INVALID INPUT'
                ) {
                    this.setState({ input: this.state.input + val });
                } else {
                    this.setState({
                        input: val
                    });
                }
            }
        }
    };

    clearInput = () => {
        this.setState({ input: '' });
    };

    add = () => {
        if (this.state.input === '' || this.state.input === 'INVALID INPUT') {
            this.setState({
                input: 'INVALID INPUT'
            });
        }
        if (
            /\d/.test(this.state.input[this.state.input.length - 1]) ||
            typeof this.state.input === 'number'
        ) {
            this.setState({
                input: this.state.input + '+'
            });
        }
    };

    subtract = () => {
        if (this.state.input === '' || this.state.input === 'INVALID INPUT') {
            this.setState({
                input: 'INVALID INPUT'
            });
        }
        if (
            /\d/.test(this.state.input[this.state.input.length - 1]) ||
            typeof this.state.input === 'number'
        ) {
            this.setState({
                input: this.state.input + '-'
            });
        }
    };

    multiply = () => {
        if (this.state.input === '' || this.state.input === 'INVALID INPUT') {
            this.setState({
                input: 'INVALID INPUT'
            });
        }
        if (
            /\d/.test(this.state.input[this.state.input.length - 1]) ||
            typeof this.state.input === 'number'
        ) {
            this.setState({
                input: this.state.input + '*'
            });
        }
    };

    divide = () => {
        if (this.state.input === '' || this.state.input === 'INVALID INPUT') {
            this.setState({
                input: 'INVALID INPUT'
            });
        }
        if (
            /\d/.test(this.state.input[this.state.input.length - 1]) ||
            typeof this.state.input === 'number'
        ) {
            this.setState({
                input: this.state.input + '/'
            });
        }
    };

    evaluate = () => {
        if (typeof this.state.input === 'number' || this.state.input === '') {
            this.setState({
                input: 'INVALID INPUT'
            });
        } else {
            try {
                this.setState({
                    input: math.evaluate(this.state.input)
                });
            } catch (err) {
                this.setState({
                    input: 'INVALID INPUT'
                });
            }
        }
    };
    deleteInput = () => {
        if (
            this.state.input !== '' &&
            this.state.input !== 'INVALID INPUT' &&
            this.state.input !== 'Infinity'
        ) {
            if (typeof this.state.input === 'number') {
                this.setState({
                    input: ''
                });
            } else {
                this.setState((prevState) => ({
                    input: prevState.input.slice(0, prevState.input.length - 1)
                }));
            }
        }
    };

    render() {
        return (
            <div className="App">
                <Video></Video>
                <div className="cals-wrapper">
                    <Input>{this.state.input}</Input>
                    <div className="row"></div>
                    <div className="row">
                        <Button handleClick={this.addToInput}>7</Button>
                        <Button handleClick={this.addToInput}>8</Button>
                        <Button handleClick={this.addToInput}>9</Button>
                        <Button handleClick={this.divide}>/</Button>
                    </div>
                    <div className="row">
                        <Button handleClick={this.addToInput}>4</Button>
                        <Button handleClick={this.addToInput}>5</Button>
                        <Button handleClick={this.addToInput}>6</Button>
                        <Button handleClick={this.multiply}>*</Button>
                    </div>
                    <div className="row">
                        <Button handleClick={this.addToInput}>1</Button>
                        <Button handleClick={this.addToInput}>2</Button>
                        <Button handleClick={this.addToInput}>3</Button>
                        <Button handleClick={this.add}>+</Button>
                    </div>
                    <div className="row">
                        <Button handleClick={this.addDecimal}>.</Button>
                        <Button handleClick={this.addZeoToInput}>0</Button>
                        <Button handleClick={this.evaluate}>=</Button>
                        <Button handleClick={this.subtract}>-</Button>
                    </div>
                    <div className="row">
                        <Back delete={this.deleteInput}></Back>
                        <ClearButton handleClear={this.clearInput}>
                            Clear
                        </ClearButton>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
